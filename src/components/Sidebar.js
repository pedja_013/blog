import React, { useEffect, useState } from "react";
import Categories from "./Categories";
import {Card} from "react-bootstrap";

function Sidebar() {
    const [appState, setAppState] = useState({
        categories: null,
    });

    useEffect(() => {
        const apiUrl = `https://frontend-api-test-nultien.azurewebsites.net/api/Category`;
        fetch(apiUrl)
            .then((res) => res.json())
            .then((categories) => {
                setAppState({ loading: false, categories: categories.resultData });
            });
    }, [setAppState]);

    return (
        <Card className="p-3 sidebar">
            <h5>Blog categories</h5>
            <Categories categories = {appState.categories} />
        </Card>
    );
}

export default Sidebar;