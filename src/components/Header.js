import React from "react";
import {Container, Form, FormControl, Nav, Navbar} from "react-bootstrap";

function Header(props) {
    return (
        <header>
            <Container>
                <Navbar expand="lg" className="navbar-dark mb-5">
                    <Navbar.Brand href="#">My Blog</Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbarScroll" />
                    <Navbar.Collapse id="navbarScroll">
                        <Form className="d-flex ms-auto">
                            <FormControl
                                type="search"
                                placeholder="Search"
                                className="me-2"
                                aria-label="Search"
                                onKeyUp={(e) => props.searchPosts(e.target.value)}
                            />
                        </Form>
                        <Nav
                            className="my-2 my-lg-0"
                        >
                            <Nav.Link href="#action1">Link1</Nav.Link>
                            <Nav.Link href="#action2">Link2</Nav.Link>
                            <Nav.Link href="#action2">Link3</Nav.Link>
                            <Nav.Link href="#action2">My profile</Nav.Link>
                            <Nav.Link href="#action2">Logout</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </Container>
        </header>
    );
}

export default Header;