import React from "react";
import SystemAlert from "./SystemAlert";
import {Button} from "react-bootstrap";

function TopContent(props) {
    return (
        <div>
            <h1 className="mb-4">Welcome to my blog</h1>
            <SystemAlert showAlert={props.showAlert} setShowAlert={props.setShowAlert} message={props.message}/>
            <div className="d-flex justify-content-end mb-4">
                <Button className="btn btn-dark ps-5 pe-5" onClick={props.handleShow}>Add new post</Button>
            </div>
        </div>
    );
}

export default TopContent;