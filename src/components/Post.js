import React from "react";
import img80 from "../assets/img/img-80.png";
import img100 from "../assets/img/img-100.png";
import {Button, Card} from "react-bootstrap";

const Post = (props) => {
    const date = props.post.createdAt.substring(0,19) + "Z"
    const dateObject = new Date(date)
    const formatedDate = dateObject.toLocaleDateString()
    const formatedTime = dateObject.toLocaleTimeString()

    return (
        <Card className="post py-3 px-4 mb-4" key={props.post.id}>
            <div className="post__header d-block d-lg-flex align-items-center justify-content-between">
                <div className="post__header-info d-block d-sm-flex mb-4 mb-lg-0">
                    <div className="post__header-img d-inline-block">
                        <img src={img80} className="me-3 mb-4 mb-sm-0" alt="placeholder"/>
                    </div>
                    <div className="post__header-title d-inline-block">
                        <h5 className="mb-0">
                            <strong>{props.post.title}</strong>
                        </h5>
                        <div>Posted date: {formatedDate} at {formatedTime} by Some person</div>
                    </div>
                </div>
                <div className="post__header-controls">
                    <Button className="btn btn-dark me-3" onClick={() => props.handleEditShow(props.post.id)}>Edit</Button>
                    <Button className="btn btn-dark" onClick={() => props.handleDelete(props.post.id)}>Delete</Button>
                </div>
            </div>
            <div className="post__content my-3">
                {props.post.text}
            </div>
            <div className="post__images d-flex justify-content-center">
                <img src={img100} className="me-2" alt="placeholder"/>
                <img src={img100} className="me-2" alt="placeholder"/>
                <img src={img100} className="me-2" alt="placeholder"/>
            </div>
        </Card>
    );
};

export default Post;