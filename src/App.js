import './App.css';
import './assets/styles/main.scss';
import Content from "./components/Content";

function App() {
  return (
      <>
        <Content />
      </>
  );
}

export default App;
